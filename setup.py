#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# vim: set et sw=4 ts=4 sts=4 ff=unix fenc=utf8:
# Author: Ryan<wangdongsheng@hylanda.com>
# Created on 2019-11-11 08:47:51


from distutils.core import setup

setup(name = "pyhlseg",
    version = "0.9.0",
    description = "Hylanda segment for Chinese",
    author = "Ryan",
    author_email = "wangdongsheng@hylanda.com",
    url = "http://www.hylanda.com",
    #Name the folder where your packages live:
    #(If you have other packages (dirs) or modules (py files) then
    #put them into the package directory - they will be found 
    #recursively.)
    packages = ['pyhlseg'],
    #'package' package must contain files (see list above)
    #I called the package 'package' thus cleverly confusing the whole issue...
    #This dict maps the package name =to=> directories
    #It says, package *needs* these files.
    package_data={
        'pyhlseg': [
            'dictionary/*.*',
            'lib/*.jar'
        ],
    },
    long_description = """Really long text here.""" ,
    install_requires=['jpype1>=0.7']

    #
    #This next part it for the Cheese Shop, look a little down the page.
    #classifiers = []     
) 
