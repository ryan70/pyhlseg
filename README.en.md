pyhlseg
========

A powerful toolkit for Chinese word segmentation in Python.

Installation
------------

* `python setup.py install`

Requirements
------------
- Python 2.7 or later. 
- jpype1 >= 0.7
	- If the system has multiple versions of JRE installed and jpype.Getdefaultjvmpath() does not return the correct version, you can directly pass in the correct path through the parameter "jvm_path" of start_jvm(). Refer to the following code to specify the correct JVM path:
	 linux: 
		`jvmPath="/usr/local/jdk/jdk1.8.0_162/jre/lib/amd64/server/libjvm.so"`
	 Windows: 
		`jvmPath='C:\\Program Files\\Java\\jdk1.8.0_144\\jre\\bin\\server\\jvm.dll'`

- Install all the dependency needed:<br/>
    `pip install --user -r requirements.txt`<br/>
        or if you fancy to have your dependency installed with root permission<br/>
    `sudo pip install -r requirements.txt`


Sample Code 
-----------

```python
from pyhlseg import *

def load_hlseg():
	HylandaSegment.start_jvm()
	HylandaSegment.load_dictionary(user_dict_path=HylandaSegment.BUILD_IN_USER_DICT)
	HylandaSegment.set_option(grain_size=GrainSize.LARGE)

def unload_hlseg():
	HylandaSegment.shutdown_jvm()

if __name__ == "__main__":
    ...

	load_hlseg()

	#read the data
	with open(args[0], 'r', encoding='utf-8') as corpus:
		for line in corpus:
			# do segment
			seg_result = HylandaSegment.segment(line)
            seg_text = str(seg_result.toString())
			#the segmentation results can be accessed using the toString (word space separated), tostringarray, or tokenarray interfaces
			#toTokenArray() returns the array of Token objests，the main members of token object include wordStr, type, subType, natureFlag, userTag, etc. 
			#for example：
			# words = list(HylandaSegment.segment(line).toStringArray())
			# tokens = list(HylandaSegment.segment(line).toTokenArray())
			#you can also use the seg_to_words() to get a list of Word

	unload_hlseg()

    ...

```

Under the test directory, there are two test routines, test.py (single thread) and test-mt.py (multi-threaded), in each of them integrates the word segmentation of Jieba and can be tested in comparison.
```
	- run test.py： 
		python test.py <corpus.txt>
	- run test-mt.py： 
		python test-mt.py [options] <corpus.txt>
			Usage: test-mt.py [options] corpus
			example: test-mt.py /data/test_corpus.txt
			Options:
  			--version             show program's version number and exit
  			-h, --help            show this help message and exit
  			-o FILE, --output=FILE
									save result to FILE
  			-t THREAD_COUNT, --thread_count=THREAD_COUNT 
									set the thread count
  			-j                    use jieba for segment
```

Related projects
-------
[Segmentation Evaluation Tool] (https://gitee.com/ryan70/segEvalTool)

A web word segmentation comparative evaluation tool made by streamlit integrates several mainstream word segmentation, which can be used for intuitive comparative evaluation of its results.

![输入图片说明](https://images.gitee.com/uploads/images/2019/1226/164813_4fb5d527_5038328.jpeg "ui.jpg")

License
-------
Licensed under the Apache License, Version 2.0



