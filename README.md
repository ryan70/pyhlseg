pyhlseg
========
海量(Hylanda)一直专注于中文信息处理技术领域的基础性研究工作，中文智能分词是其重点研发的技术之一，pyhlseg是基于海量智能分词5.x(java版)的python封装。

主要功能
--------
- **词形切分**
对给定的字序列文本，自动切分为词序列文本。
- **支持自定义词典功能**
对于新词、专业词汇等可加入到自定义词典中，同时支持自定义词属性。
- **支持三种颗粒度的分词结果**
为适应各种不同应用场景，支持一次分词后获得多粒度的分词结果。海量系统现在提供了三种颗粒度的分词结果：一是普通颗粒度，也是默认颗粒度，主要常规的各种应用情况；二是大颗粒度，通常用于自动分类、信息挖潜、机器翻译、语音合成、人工智能等领域，用于提升信息分析的有效性和准确性；三是小颗粒度，主要用于信息检索领域，用于提升查全率。
例如：
对"天津海量信息技术股份有限公司"进行分词：
普通颗粒度的结果为：天津 海量 信息 技术 股份 有限公司
大颗粒度分词结果为：天津海量信息技术股份有限公司
小颗粒度分词(检索优化)结果为：天津 海量 信息 技术 股份 有限 公司
- **支持计算一段文字的关键词**
可提取跟文字内容相关的词
- **支持计算一段文字的语义指纹**
计算一段文字的语义指纹，唯一标识这段文字，用于进行文字内容的消重。
- **支持词性标注**
可为每个分词结果标识分词的数据，如：判断某个词是名词还是动词
- **支持面向检索的结果优化**
若需要提升检索的召回率，可以采用小颗粒输出，我们针对检索的业务情境对分词结果进行了优化。

安装
------------

* `python setup.py install`

依赖
------------
- Python 2.7 or later. 
- jpype1 >= 0.7
	- 如果系统安装了多个版本的jre，jpype.getDefaultJVMPath()返回的又不是正确的版本，则可以直接通过start_jvm()的参数jvm_path传入正确的路径。可参考下面的代码指定正确的jvmPath：
	 linux: 
		`jvmPath="/usr/local/jdk/jdk1.8.0_162/jre/lib/amd64/server/libjvm.so"`
	 Windows: 
		`jvmPath='C:\\Program Files\\Java\\jdk1.8.0_144\\jre\\bin\\server\\jvm.dll'`

- Install all the dependency needed:<br/>
    `pip install --user -r requirements.txt`<br/>
        or if you fancy to have your dependency installed with root permission<br/>
    `sudo pip install -r requirements.txt`


例程 
-----------

```python
from pyhlseg import *

#加载分词
def load_hlseg():
	HylandaSegment.start_jvm()
	HylandaSegment.load_dictionary(user_dict_path=HylandaSegment.BUILD_IN_USER_DICT)
	HylandaSegment.set_option(grain_size=GrainSize.LARGE)

#卸载分词
def unload_hlseg():
	HylandaSegment.shutdown_jvm()

if __name__ == "__main__":
    ...

	#加载分词
	load_hlseg()

	#读取文件
	with open(args[0], 'r', encoding='utf-8') as corpus:
		for line in corpus:
			# 分词
			seg_result = HylandaSegment.segment(line)
            seg_text = str(seg_result.toString())
			#分词结果通常可以使用toString(词与词空格分隔)、toStringArray或toTokenArray接口访问
			#toTokenArray获得的是Token对象数组，Token对象的主要成员有wordStr、type、subType、
			# natureFlag、userTag等、具体可以参考海量分词说明文档
			#例如：
			# words = list(HylandaSegment.segment(line).toStringArray())
			# tokens = list(HylandaSegment.segment(line).toTokenArray())
			# 也可以使用seg_to_words方法得到Word类型的列表

	#卸载分词
	unload_hlseg()

    ...

```

test目录下有两个测试例程test.py(单线程)和test-mt.py(多线程)，并集成了结巴分词，以方便对比测试。
```
	- 运行test.py的命令： 
		python test.py [options] <corpus.txt>
  			-o FILE, --output=FILE
									save result to FILE
  			-j                    use jieba for segment
			
	- 运行test-mt.py的命令： 
		python test-mt.py [options] <corpus.txt>
			Usage: test-mt.py [options] corpus
			example: test-mt.py /data/test_corpus.txt
			Options:
  			--version             show program's version number and exit
  			-h, --help            show this help message and exit
  			-o FILE, --output=FILE
									save result to FILE
  			-t THREAD_COUNT, --thread_count=THREAD_COUNT 
									set the thread count
  			-j                    use jieba for segment
```

相关项目
-------
[分词评测工具](https://gitee.com/ryan70/segEvalTool)
使用streamlit做的一个web分词对比评测工具，集成了多个主流分词，可对其结果进行直观的对比评测。

![输入图片说明](https://images.gitee.com/uploads/images/2019/1226/164813_4fb5d527_5038328.jpeg "ui.jpg")

License
-------
Licensed under the Apache License, Version 2.0



